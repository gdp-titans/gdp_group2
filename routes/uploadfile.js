var XLSX = require('xlsx');
var formidable = require('formidable');
var express = require('express');
var router = express.Router();
var isLoggedIn = require('../config/authenticate').isLoggedIn;
var utils = require('../utils/Utilities');
var uploadUtils = require('../utils/UploadUtil');

router.get('/', isLoggedIn, function (req, res, next) {
    if (req.session.user['ROLE'] != "admin") {
      res.redirect('/');
    }
    console.log("Loading Page upload file")
    res.render('./fileupload/fileUpload.ejs',
        {
            title: 'Upload File',
            user: req.session.user,
            layout: 'layout.ejs',
            message: '',
            uploadFileID: ''
        });
});

router.post('/', isLoggedIn, function (req, res) {
    if (req.session.user['ROLE'] != "admin") {
      res.redirect('/');
    }
    var upID;
    console.log("Loading Page upload file");
    (async() => {
        console.log("Entered Async")
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            var f = files[Object.keys(files)[0]];
            var wb = XLSX.readFile(f.path);
            uploadUtils.generateUploadID(function (uploadId) {
                upID = uploadId;
                console.log("ID is: " + uploadId);
                uploadUtils.addUploadFile(uploadId, f.name, req.session.user['USERNAME'], function (result) {
                    uploadUtils.parseExcelData(wb,uploadId);
                    console.log("Upload completed");
                });
            })
        });
    })();
    res.render('./fileupload/fileUpload.ejs',
        {
            title: 'Upload File',
            user: req.session.user,
            uploadFileID: upID,
            layout: 'layout.ejs',
            message: 'File uploaded sucessfully and is processing.'
        });
});


module.exports = router;