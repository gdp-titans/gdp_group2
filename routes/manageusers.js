var express = require('express');
var router = express.Router();
var isLoggedIn = require('../config/authenticate').isLoggedIn;
var utils = require('../utils/Utilities');

/* GET All users. */
router.get('/users', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  let query = "select username,first_name,last_name,role from users where username not in ('root','"+req.session.user['USERNAME']+"')";
  let param = []
  console.log("Loading Page New Users" + param)
  utils.getDataObjects(query, param, function (err, data) {
    if (data) {
      console.log("Inside data" + data.length)
      res.render('./manageusers/viewUsers.ejs',
        {
          title: 'SSCRA',
          user: req.session.user,
          layout: 'layout.ejs',
          msg: req.flash('succcessAdd'),
          err: req.flash('failureAdd'),
          users: data,
          datamsg: ''
        });
    } else {
      res.render('./manageusers/viewUsers.ejs',
        {
          title: 'SSCRA',
          user: req.session.user,
          layout: 'layout.ejs',
          msg: req.flash('succcessAdd'),
          err: req.flash('failureAdd'),
          users: null,
          datamsg: 'Unable to retrieve users data from database, please try later.'
        });
    }
  });

});

router.post('/sendInvites', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  console.log(req.body.emails)
  utils.sendInvites(req.body.emails)
  req.flash('succcessAdd','Invitaions to new users will be sent shortly.')
  res.redirect('/manageusers/users')
});

router.get('/addUser', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  console.log("Loading Page New Users")
  res.render('./manageusers/addUser.ejs',
    {
      title: 'Add New User',
      newUser: ['', '', '', '', '', '', ''],
      user: req.session.user,
      layout: 'layout.ejs',
      err: ''
    });
});

router.post('/addUser', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  var newU = [];
  newU.push(req.body.username); newU.push(req.body.password); newU.push(req.body.first_name);
  newU.push(req.body.last_name); newU.push(req.body.email); newU.push(req.body.role);
  console.log("data: " + newU)
  utils.addNewUser(newU, function (status) {
    console.log(status)
    if (status == 'Exists') {
      res.render('./manageusers/addUser.ejs',
        {
          title: 'Add New User',
          newUser: newU,
          user: req.session.user,
          layout: 'layout.ejs',
          err: "Username already used, please  use a different username."
        });
    } else if (status == 'Success') {
      req.flash('succcessAdd', 'User added successfully!')
      res.redirect('/manageusers/users')
    } else {
      req.flash('failureAdd', 'Something went wrong. Unable to add user, please try later.')
      res.redirect('/manageusers/users')
    }
  });
});


/* GET All users. */
router.get('/user/:id', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  let query = "select username,first_name,last_name,role,email from users where username != 'root' and username = :1";
  let param = [req.params.id]
  console.log("Loading Page New Users" + param)
  utils.getDataObjects(query, param, function (err, data) {
    if (data != null && data != []) {
      console.log("Inside data element")
      console.log(data[0]['USERNAME'])
      res.render('./manageusers/editUser.ejs',
      {
        title: 'User Details',
        newUser: data[0],
        user: req.session.user,
        layout: 'layout.ejs',
        err: ''
      });
    } else {
      res.render('./manageusers/editUser.ejs',
        {
          title: 'User Details',
          newUser: ['', '', '', '', '', '', ''],
          user: req.session.user,
          layout: 'layout.ejs',
          err: 'Unable to load user details, please refresh the page or try later.'
        });
    }
  });
});
///manageusers/delete/

router.get('/delete/:id', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  utils.executeQuery("delete from users where username = :1",[req.params.id], function(err, result){
    if(err){
      req.flash('failureAdd', 'Something went wrong. Unable to remove user, please try later.')
    }
    else {
      req.flash('succcessAdd', 'User removed successfully!')
    }
    res.redirect('/manageusers/users');
  })
});

router.post('/editUser/:id', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  var newU = [];
  newU.push(req.body.username);  newU.push(req.body.first_name);
  newU.push(req.body.last_name); newU.push(req.body.role); newU.push(req.body.email);
  let n = {
    USERNAME: req.body.username,
    FIRST_NAME: req.body.first_name,
    LAST_NAME: req.body.last_name,
    ROLE: req.body.role,
    EMAIL: req.body.email,
  } 
  console.log("data: " + newU)
  utils.updateUser(newU, req.params.id, function (status) {
    console.log(status)
    if (status == 'Exists') {
      res.render('./manageusers/editUser.ejs',
        {
          title: 'Update User',
          newUser: n,
          user: req.session.user,
          layout: 'layout.ejs',
          err: "Username already used, please  use a different username."
        });
    } else if (status == 'Success') {
      req.flash('succcessAdd', 'User updated successfully!')
      res.redirect('/manageusers/users')
    } else {
      req.flash('failureAdd', 'Something went wrong. Unable to update user, please try later.')
      res.redirect('/manageusers/users')
    }
  });
});

router.get('/register/:id',function(req, res){
  req.logOut();
  console.log(utils.encryptData("register", "asd"))
  utils.getDataObjects("select reset_token from users where reset_token=:1 and username is null",
		[utils.decryptData("register",req.params.id)], function(error, result){
				console.log(result)
        var len = 0;
				if(result && result.length>0){
          len = result.length;
					console.log(error+" in reset password "+len)
        }
        res.render('./manageusers/register.ejs',
        { title: 'Register User',
          registerId: req.params.id,
          length: len, 
          userInfo: {},
          err: req.flash('registerErr')
        });
		});
});

router.post('/register/:id',function(req, res){
  var newU = []
  newU.push(req.body.username); newU.push(req.body.first_name); newU.push(req.body.last_name); newU.push(req.body.password);
  utils.registerUser(newU, utils.decryptData("register", req.params.id), function(result){
    if(result == "Success") {
      req.flash('loginSucc', 'Registration successful, please login.');
      res.redirect('/auth/login');
    } else {
      var e = "Somethig went wrong, please try again later. If the issue still persists please contact administrator.";
      if(result == "Exists")
        e = "Username already taken, please use a different username."
      res.render('./manageusers/register.ejs',
        { title: 'Register User',
          registerId: req.params.id,
          length: 1, 
          userInfo: {fname:req.body.first_name, lname:req.body.last_name},
          err: e
      });
    }
  })
});


module.exports = router;