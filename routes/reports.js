var express = require('express');
var router = express.Router();
var isLoggedIn = require('../config/authenticate').isLoggedIn;
var utils = require('../utils/Utilities');
var meta = require('../config/metaData');
var reportUtils = require('../utils/ReportUtil');

/* GET All users. */
router.get('/', isLoggedIn, function (req, res, next) {
    console.log(req)
    let query = "select up.upload_id, (u.FIRST_NAME||' '||u.LAST_NAME) as NAME, up.filename, up.UPLOAD_TIME, up.status from uploads up, users u where u.username = up.username order by up.UPLOAD_TIME desc";
    let param = []
    console.log("Loading Page reports: ")
    utils.getDataObjects(query, param, function (err, data) {
      if (data) {
        res.render('./reports/reports.ejs',
          {
            title: 'SSCRA',
            user: req.session.user,
            layout: 'layout.ejs',
            uploads: data,
            datamsg: req.flash('datamsg'),
          });
      }
    });
  });

  router.get('/:id', isLoggedIn, function (req, res, next) {
    console.log(req.params.id)
    reportUtils.logAction('Report',req.session.user['EMAIL'],'/reports'+req.path,req.params.id);
    res.render('./reports/report.ejs',
      {
        title: 'SSCRA',
        user: req.session.user,
        layout: './reports/layout.ejs',
        sheets: meta['Sheets'],
        reports: meta['Reports'],
        reportId: req.params.id,
      });
  });

    
  router.get('/delete/:id', isLoggedIn, function (req, res, next) {
    if (req.session.user['ROLE'] != "admin") {
      res.redirect('/');
    }
    utils.executeQuery("BEGIN DELETEUPLOADDATA(:1); END;", [req.params.id], function(err,result){
      if(result)
      req.flash('datamsg', 'Report data cleared sucessfully!');
      else 
      req.flash('datamsg', 'Unable to process your request curently, please retry later.');
      res.redirect('/reports/')
    });
  });

router.get('/:id/sheet/:sheetname', isLoggedIn, function (req, res, next) {
  if (req.session.user['ROLE'] != "admin") {
    res.redirect('/');
  }
  reportUtils.logAction(req.params.sheetname, req.session.user['EMAIL'],'/reports'+req.path,req.params.id);
  console.log(req.params.sheetname)
  reportUtils.getSheetData(req.params.id, req.params.sheetname, function(err, result, headings, keys){
    var error
    if(err){
      if(err == 'NoSheet')
        error = "Incorrect url, please verify url and try again";
      else 
          error = "Sorry, we are unable to process your request currently. Please retry or contact administrator if the issue still persists";
    }
    res.render('./reports/dataTable.ejs',
      {
        dataTitle: req.params.sheetname,
        user: req.session.user,
        layout: './reports/layout.ejs',
        sheets: meta['Sheets'],
        reports: meta['Reports'],
        reportId: req.params.id,
        err: error,
        data: result,
        dataHeadings: headings,
        dataKeys: keys,
    });
  })
});


router.get('/:id/generate/:reportname', isLoggedIn, function (req, res, next) {
  reportUtils.logAction(req.params.reportname, req.session.user['EMAIL'],'/reports'+req.path,req.params.id);
  console.log(req.params.reportname)
  reportUtils.getReportData(req.params.id, req.params.reportname, function(err, result, headings, keys, totals){
    var error
    if(err){
      if(err == 'NoReport')
        error = "Incorrect url, please verify url and try again";
      else 
          error = "Sorry, we are unable to process your request currently. Please retry or contact administrator if the issue still persists";
    }
    console.log(result)
    res.render('./reports/reportTable.ejs',
      {
        dataTitle: req.params.reportname,
        user: req.session.user,
        layout: './reports/layout.ejs',
        sheets: meta['Sheets'],
        reports: meta['Reports'],
        reportId: req.params.id,
        err: error,
        data: result,
        dataHeadings: headings,
        dataKeys: keys,
        tot: totals,
    });
  })
});

module.exports = router;






  /* GET All users. 
router.get('/:id/:params', isLoggedIn, function (req, res, next) {
    if (req.session.user['ROLE'] != "admin") {
      res.redirect('/');
    }
    
    let query = "select schedule_owner,count(*) as Cancelled, (select count(*) from appointments where event != 'CANCELLED' and student_id is not null and student_id != '------' and schedule_owner = ap.schedule_owner and upload_id = :1) as CREATED from appointments ap where event = 'CANCELLED' and student_id is not null and student_id != '------' and ap.upload_id = :1 group by ap.schedule_owner";
    let param = [req.params.id]
    console.log("Loading Page report " + param)
    utils.getDataObjects(query, param, function (err, data) {
        if (data) {
          console.log("Inside report data" + data.length)
          res.render('./reports/report.ejs',
            {
              title: 'SSCRA',
              user: req.session.user,
              layout: './reports/layout.ejs',
              uploads: data,
              datamsg: '',
              sheets: meta['Sheets'],
              reportId: req.params.id,
            });
        }
      });
  });
  */