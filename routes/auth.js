var LOGGER = require('log4js').getLogger("routes");
var isLoggedIn = require('../config/authenticate').isLoggedIn;
var utils = require('../utils/Utilities');

module.exports = function(app, passport) {

	app.get('/', function(req, res) {
		res.redirect('/home'); 
    });
    
	app.get('/auth/login', function(req, res) {
		if(req.isAuthenticated())
			res.redirect('/home')
        res.render('./user/login.ejs', { 
			message: req.flash('loginMessage'),
			succ: req.flash('loginSucc') 
		});
    });

	app.post('/auth/login', passport.authenticate('local-login', {
            successRedirect : '/home',
            failureRedirect : '/auth/login',
            failureFlash : true }),
		function (req, res) {
			console.log('in post in')
			if (req.body.remember) {
				req.session.cookie.maxAge = 1000 * 60 * 3;
			} else {
				req.session.cookie.expires = false;
			}
			res.redirect('/');
		});
    
	app.get('/home', isLoggedIn, function(req, res) {
		utils.getDataObjects("select * from logs where email = :1 and rownum<11 order by UPDATE_TIME desc",[req.session.user['EMAIL']],function(err1,result1){
			utils.getDataObjects("select * from uploads where username = :1 and rownum<11 order by UPLOAD_TIME desc",[req.session.user['USERNAME']],function(err2,result2){
				res.render('index.ejs', {
					user : req.session.user,
					recent: result1,
					myUploads: result2,
					recentErr: err1,
					myErr: err2,
				});
			});
		})
	});

	app.get('/profile', isLoggedIn, function (req, res) {
		utils.getDataObjects("select username,first_name,last_name,role,email from users where email=:1",[req.session.user['EMAIL']], 
			function(err, result){
				var u = req.session.user
				if(err) {
				console.log("In profile error: "+err)
				} else if(result.length && result.length>0) {
					u = result[0];
				}
				res.render('./user/profile.ejs', {
					title: 'Update Profile',
					user: u,
					layout: 'layout.ejs',
					msg: req.flash('succcessAdd'),
					err: req.flash('failureAdd')
				}); 
		})
	});

	app.get('/auth/logout', function(req, res) {
		req.logout();
		res.redirect('/auth/login');
    });
    
	app.post('/updateProfile', isLoggedIn, function (req, res, next) {
		var newU = [];
		newU.push(req.body.username); newU.push(req.body.first_name); newU.push(req.body.last_name);
		console.log("data: " + newU)
		utils.updateProfile(newU, req.session.user['USERNAME'], function (status) {
			console.log(status)
			if (status == 'Exists') {
				req.flash('failureAdd', 'Username already used, please  use a different username.')
				res.redirect('/profile')
			} else if (status == 'Success') {
				req.flash('succcessAdd', 'Profile updated successfully!')
				res.redirect('/profile')
			} else {
				req.flash('failureAdd', 'Something went wrong. Unable to update profile, please try later.')
				res.redirect('/profile')
			}
		});
	});

	app.post('/updatePassword', isLoggedIn, function (req, res, next) {
		utils.updatePassword(req.body.newPassword, req.body.oldPassword, req.session.user['EMAIL'], function (status) {
			console.log(status)
			if (status == 'IncorrectOld') {
				req.flash('failureAdd', 'Incorrect old password, please provide new password and try again.')
				res.redirect('/profile')
			} else if (status == 'Success') {
				req.flash('succcessAdd', 'Password updated successfully!')
				res.redirect('/profile')
			} else {
				req.flash('failureAdd', 'Something went wrong. Unable to update password, please try later.')
				res.redirect('/profile')
			}
		});
	});

	app.get('/auth/forgotPassword', function(req, res){
		res.render('./user/forgotPassword.ejs', { 
			message: req.flash('forgotPassMsg') 
		});
	});

	app.post('/auth/forgotPassword', function (req, res) {
		var msg = 'There is no account available with the username or email id provided. Please verify details and try again.'
		utils.getDataObjects("select email,FIRST_NAME from users where email=:1 or username=:1 and username is not null", [req.body.username], function (err, result) {
			console.log(result)
			if (result && result.length > 0) {
				console.log(result[0]['EMAIL'])
				msg = 'An email will be sent to ' + result[0]['EMAIL']
					+ ' shortly. Please follow the instructions provided in the email to reset your password.'
					+ '\nIf you are unable to find email in your inbox please check your spam folders.'
				utils.sendForgotPasswordMail(result[0]['EMAIL'], result[0]['FIRST_NAME']);
			}
			req.flash('forgotPassMsg', msg)
			res.redirect('/auth/forgotPassword')
		});
	});
	
	app.get('/auth/resetpassword/:id', function(req, res){
		req.logOut();
		utils.getDataObjects("select reset_token from users where reset_token=:1", [utils.decryptData("reset", req.params.id)],
			function (error, result) {
				console.log(result)
				var len = 0;
				if (result && result.length > 0) {
					len = result.length;
					console.log(error + " in reset password " + len)
					console.log(result)
				}
				res.render('./user/resetPassword.ejs', {
					err: error,
					length: len,
					resetToken: req.params.id
				});
			});
	});

	app.post('/auth/resetpassword/:id', function(req, res){
		utils.executeUpdateQuery('update users set password = :1, reset_token=null where reset_token=:2', [req.body.newPassword, utils.decryptData("reset", req.params.id)], function (error, result) {
			if (error || (result && result.rowsAffected<1))
				req.flash('loginSucc', 'Unable to process your request to reset password. Please try later');
			else
				req.flash('loginSucc', 'Password reset sucessfull. Please login with your new password.');
			console.log("After password reset " + result.rowsAffected)
			res.redirect('/auth/login');
		});
	});
};



