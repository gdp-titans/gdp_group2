var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var expressLayouts = require('express-ejs-layouts');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
require('./config/passport')(passport);

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(expressLayouts)
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: 'sscra',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./routes/auth.js')(app, passport);
app.use('/manageusers', require('./routes/manageusers'))
app.use('/upload', require('./routes/uploadfile'))
app.use('/reports', require('./routes/reports'))

// catch 404 and forward to error handler
app.use(function (req, res) {
  res.status(404).render('404.ejs',
    {
      title: 'Error',
      user: req.session.user,
      layout: 'layout.ejs'
    });
});

/*app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});*/

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(err)
  res.status(err.status || 500);
  res.render('error',
    {
      title: 'Error',
      user: req.session.user,
      layout: 'layout.ejs'
    });
});

module.exports = app;
