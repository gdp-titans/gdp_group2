module.exports = {
    'Sheets':['Activity' ,'Appointments' ,'Office Hours' ,'Profiles' ,'Intake' ,'Group Sessions' ,'Prospects' ,'Flag Definitions' ,'Plans' ,'Speed Notes' ,'Events' ,'Attendance' ,'Flags' ,'Student Report'],
    'Tables':['ACTIVITY' ,'APPOINTMENTS' ,'OFFICE_HOURS' ,'PROFILES' ,'INTAKE' ,'GROUP_SESSIONS' ,'PROSPECTS' ,'FLAG_DEFINITIONS' ,'PLANS' ,'SPEED_NOTES' ,'EVENTS' ,'ATTENDANCE' ,'FLAGS' ,'STUDENT_REPORT'],
    'ActivitySheet' : ['Date', 'Office Hours Created', 'Total Appointments Created', 'Appointments Created By Student', 'Appointments Created By Office Hour Owner', 'Prospective Appointments', 'Appointments Updated', 'Appointments In Range', 'Group Sessions Created', 'Total Spots Filled', 'Spots Filled By Student', 'Spots Filled By Session Owner', 'Spots In Range', 'Events Uploaded', 'Number of Attendees', 'Intake Forms Created', 'Intake Forms Updated', 'Profiles Created', 'Profiles Updated', 'Photos Uploaded', 'Flags Raised Automatically', 'Flags Raised Manually', 'Flags Raised', 'Flags Cleared', 'Academic Plans Created' ]
    ,'ActivityTable' : ['EVENT_DATE' ,'OFFICE_HOURS_CREATED' ,'TOTAL_APPOINTMENTS_CREATED' ,'APPOINTMENTS_CREATEDBY_STUDENT' ,'APTMENTS_CREATEDBY_OH_OWNER' ,'PROSPECTIVE_APPOINTMENTS' ,'APPOINTMENTS_UPDATED' ,'APPOINTMENTS_IN_RANGE' ,'GROUP_SESSIONS_CREATED' ,'TOTAL_SPOTS_FILLED' ,'SPOTS_FILLEDBY_STUDENT' ,'SPOTS_FILLEDBY_SESSION_OWNER' ,'SPOTS_IN_RANGE' ,'EVENTS_UPLOADED' ,'NUMBER_OF_ATTENDEES' ,'INTAKE_FORMS_CREATED' ,'INTAKE_FORMS_UPDATED' ,'PROFILES_CREATED' ,'PROFILES_UPDATED' ,'PHOTOS_UPLOADED' ,'FLAGS_RAISED_AUTOMATICALLY' ,'FLAGS_RAISED_MANUALLY' ,'FLAGS_RAISED' ,'FLAGS_CLEARED' ,'ACADEMIC_PLANS_CREATED']
    ,'ProfilesSheet' : ['Date', 'Event', 'Name', 'ID', 'Integration ID', 'Username', 'Email', 'Role', 'Photo']
    ,'ProfilesTable' : ['EVENT_DATE' ,'EVENT' ,'NAME' ,'ID' ,'INTEGRATION_ID' ,'USERNAME' ,'EMAIL' ,'ROLE' ,'PHOTO']
    ,'AppointmentsSheet' : ['Event Date', 'Event', 'Schedule Owner', 'Schedule Block Name', 'Schedule Block Type', 'Student Name', 'Student ID', 'Student Integration ID', 'Student Username', 'Student Email', 'Prospective Student', 'Type', 'Appointment Type', 'Appointment Reason', 'Appointment Location', 'Appointment Activities', 'Appointment Cancelled By', 'Comment Date', 'Comment']
    ,'AppointmentsTable' : ['EVENT_DATE', 'EVENT', 'SCHEDULE_OWNER', 'SCHEDULE_BLOCK_NAME', 'SCHEDULE_BLOCK_TYPE', 'STUDENT_NAME', 'STUDENT_ID', 'STUDENT_INTEGRATION_ID', 'STUDENT_USERNAME', 'STUDENT_EMAIL', 'PROSPECTIVE_STUDENT', 'TYPE', 'APPOINTMENT_TYPE', 'APPOINTMENT_REASON', 'APPOINTMENT_LOCATION', 'APPOINTMENT_ACTIVITIES', 'APPOINTMENT_CANCELLED_BY', 'COMMENT_DATE', 'COMMENTS']
    ,'Office HoursSheet' : ['Created Date','Name','Office Hour Title','Role','Total Appointments']
    ,'Office HoursTable' : ['CREATED_DATE' ,'NAME' ,'OFFICE_HOUR_TITLE' ,'ROLE' ,'TOTAL_APPOINTMENTS']
    ,'Group SessionsSheet' : ['Created Date', 'Name', 'Session Title', 'Role', 'Max Participants', 'Total Spots Filled']
    ,'Group SessionsTable' : ['CREATED_DATE' ,'NAME' ,'SESSION_TITLE' ,'ROLE' ,'MAX_PARTICIPANTS' ,'TOTAL_SPOTS_FILLED']
    ,'EventsSheet' : ['Date', 'Name', 'Event Title', 'Reason', 'Number of Attendees' ]
    ,'EventsTable' : ['EVENT_DATE' ,'NAME' ,'EVENT_TITLE' ,'REASON' ,'NUMBER_OF_ATTENDEES']
    ,'PlansSheet' : ['Event', 'Plan Type', 'Event Date', 'Plan', 'Template Name', 'Creator Name', 'Creator Integration ID', 'Creator Username', 'Creator Email', 'Status', 'Student Name', 'Student ID', 'Student Integration ID', 'Student Username', 'Student Email' ]
    ,'PlansTable' : ['EVENT' ,'PLAN_TYPE' ,'EVENT_DATE' ,'PLAN' ,'TEMPLATE_NAME' ,'CREATOR_NAME' ,'CREATOR_INTEGRATION_ID' ,'CREATOR_USERNAME' ,'CREATOR_EMAIL' ,'STATUS' ,'STUDENT_NAME' ,'STUDENT_ID' ,'STUDENT_INTEGRATION_ID' ,'STUDENT_USERNAME' ,'STUDENT_EMAIL']
    ,'IntakeSheet' : ['Event', 'Event Date', 'Editor Name', 'Editor Integration ID', 'Editor Username', 'Editor Email', 'Student Name', 'Student ID', 'Student Integration ID', 'Student Username', 'Student Email' ]
    ,'IntakeTable' : ['EVENT' ,'EVENT_DATE' ,'EDITOR_NAME' ,'EDITOR_INTEGRATION_ID' ,'EDITOR_USERNAME' ,'EDITOR_EMAIL' ,'STUDENT_NAME' ,'STUDENT_ID' ,'STUDENT_INTEGRATION_ID' ,'STUDENT_USERNAME' ,'STUDENT_EMAIL']
    ,'ProspectsSheet' : ['Student First Name', 'Student Last Name', 'Date Created', 'Merged', 'Merged Integration ID', 'Merged Date', 'Merged By', 'Notes Count', 'Meetings Count' ]
    ,'ProspectsTable' : ['STUDENT_FIRST_NAME' ,'STUDENT_LAST_NAME' ,'DATE_CREATED' ,'MERGED' ,'MERGED_INTEGRATION_ID' ,'MERGED_DATE' ,'MERGED_BY' ,'NOTES_COUNT' ,'MEETINGS_COUNT']
    ,'Speed NotesSheet' : ['Meeting Date', 'Meeting Time', 'Student Name', 'Provider Name', 'Appointment Reason', 'Speed Note' ]
    ,'Speed NotesTable' : ['MEETING_DATE' ,'MEETING_TIME' ,'STUDENT_NAME' ,'PROVIDER_NAME' ,'APPOINTMENT_REASON' ,'SPEED_NOTE']
    ,'FlagsSheet' : ['Date', 'Event', 'Type', 'Flag', 'Closure Reason', 'Closure Reason Category', 'Raiser/Clearer', 'Student Name', 'Student ID', 'Student Integration ID', 'Student Username', 'Student Email']
    ,'FlagsTable' : ['EVENT_DATE' ,'EVENT' ,'TYPE' ,'FLAG' ,'CLOSURE_REASON' ,'CLOSURE_REASON_CATEGORY' ,'RAISER' ,'STUDENT_NAME' ,'STUDENT_ID' ,'STUDENT_INTEGRATION_ID' ,'STUDENT_USERNAME' ,'STUDENT_EMAIL']
    ,'Flag DefinitionsSheet' : ['Flag Category', 'Flag Name', 'Flag Description', 'Trigger', 'Status']
    ,'Flag DefinitionsTable' : ['FLAG_CATEGORY' ,'FLAG_NAME' ,'FLAG_DESCRIPTION' ,'TRIGGER1' ,'STATUS']
    ,'AttendanceSheet' : ['Student Name', 'Student ID', 'Student Integration ID', 'Student Username', 'Student Email', 'Course', 'Course ID', 'Attendance Date', 'Attendance Time', 'Attendance Status', 'Created By', 'Last Updated By', 'Created/Last Updated Date']
    ,'AttendanceTable' : ['STUDENT_NAME' ,'STUDENT_ID' ,'STUDENT_INTEGRATION_ID' ,'STUDENT_USERNAME' ,'STUDENT_EMAIL' ,'COURSE' ,'COURSE_ID' ,'ATTENDANCE_DATE' ,'ATTENDANCE_TIME' ,'ATTENDANCE_STATUS' ,'CREATED_BY' ,'LAST_UPDATED_BY' ,'LAST_UPDATED_DATE']
    ,'Student ReportSheet' : ['Student Name', 'Student ID', 'Student Integration ID', 'Student Username', 'Student Email', 'Student Phone', 'Student Cell Phone', 'Prospective Student', 'Note Date', 'Note Type', 'Subject', 'Note Content', 'Note Author', 'Author ID', 'Author Email', 'Author Phone', 'Author Cell Phone', 'Flag Name', 'Flag Raised Date', 'Flag Cleared Date', 'Flag Cleared By', 'Closure Reason', 'Closure Category', 'Email Student Date']
    ,'Student ReportTable' : ['STUDENT_NAME' ,'STUDENT_ID' ,'STUDENT_INTEGRATION_ID' ,'STUDENT_USERNAME' ,'STUDENT_EMAIL' ,'STUDENT_PHONE' ,'STUDENT_CELL_PHONE' ,'PROSPECTIVE_STUDENT' ,'NOTE_DATE' ,'NOTE_TYPE' ,'SUBJECT' ,'NOTE_CONTENT' ,'NOTE_AUTHOR' ,'AUTHOR_ID' ,'AUTHOR_EMAIL' ,'AUTHOR_PHONE' ,'AUTHOR_CELL_PHONE' ,'FLAG_NAME' ,'FLAG_RAISED_DATE' ,'FLAG_CLEARED_DATE' ,'FLAG_CLEARED_BY' ,'CLOSURE_REASON' ,'CLOSURE_CATEGORY' ,'EMAIL_STUDENT_DATE']

    ,'Reports':['Appointments Report','Flags Usage Report', 'Speed Notes Usage Report']
    ,'AdminReports':['Student Flags Report']
    ,'Appointments Report':
        {
            headings:['Schedule Owner', 'Created/Updated Appointments', 'Cancelled Appointments', 'Total Appointments']
            ,keys:['SCHEDULE_OWNER', 'CREATED', 'CANCELLED' ]
            ,totals: ['CREATED', 'CANCELLED']
            ,query:"select schedule_owner,count(*) as Cancelled, (select count(*) from appointments where event != 'CANCELLED' and student_id is not null and student_id != '------' and schedule_owner = ap.schedule_owner and upload_id = :1) as CREATED from appointments ap where event = 'CANCELLED' and student_id is not null and student_id != '------' and ap.upload_id = :1 group by ap.schedule_owner"
        }
    ,'Student Flags Report':
        {
            headings:['Student Name', 'Flag Type', 'Flags Count']
            ,keys:['STUDENT_NAME', 'FLAG', 'FLAG_COUNT' ]
            ,query:"select student_name,flag,count(*) as flag_count from flags where upload_id = :1 group by student_name,flag"
        }
    ,'Flags Usage Report':
        {
            headings:['Raiser/Clearer', 'Flags Raised', 'Flags Cleared', 'Total Flags']
            ,keys:['RAISER', 'RAISED', 'CLEARED' ]
            ,totals: ['RAISED', 'CLEARED']
            ,query:"select f.raiser, nvl((select count(*) from FLAGS where f.raiser = raiser and upload_id = :1 and event = 'RAISED' group by raiser),0) as raised,  nvl((select count(*) from FLAGS where f.raiser = raiser and upload_id = :1 and event = 'CLEARED' group by raiser),0) as cleared from FLAGS f where upload_id = :1  group by raiser"
        }
    ,'Speed Notes Usage Report':
        {
            headings:['Raiser/Clearer', 'Flags Raised', 'Flags Cleared', 'Total Flags']
            ,keys:['RAISER', 'RAISED', 'CLEARED' ]
            ,totals: ['RAISED', 'CLEARED']
            ,query:"select f.raiser, nvl((select count(*) from FLAGS where f.raiser = raiser and upload_id = :1 and event = 'RAISED' group by raiser),0) as raised,  nvl((select count(*) from FLAGS where f.raiser = raiser and upload_id = :1 and event = 'CLEARED' group by raiser),0) as cleared from FLAGS f where upload_id = :1  group by raiser"
        }
}