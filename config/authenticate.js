module.exports = {
  isLoggedIn(req, res, next) {
    console.log('in logged in')
    if (req.isAuthenticated())
      return next();
    res.redirect('/auth/login');
  }

}
