const passport = require('passport')
var LocalStrategy   = require('passport-local').Strategy;
var md5 = require('md5');
var LOGGER = require('log4js').getLogger("sms");
var oracledb = require("oracledb");
var db = require("../config/oracledb.js");
var utils = require('../utils/Utilities');
process.env.UV_THREADPOOL_SIZE = 100;

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        
	done(null, user['USERNAME']);
    });

    passport.deserializeUser(function(username, done) {       
		var selectSQL = "select username,first_name,last_name,role,email from users where username=:1";
		db.doConnect(function(err, connection){
			if (err) {                  
				return done(err,null);
			} else {
				db.doExecute(connection, selectSQL,[username],function(err, result) {
					if (err) {
						db.doRelease(connection);
						done(err,null);
					} else {
						LOGGER.debug('GOT RESULT');
						db.doRelease(connection);
						done(null,result.rows[0]);
					}
				});
			}
		});
	});

    passport.use(
        'local-login',
        new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true 
        },
			function(req, username, password, done) { 
				var selectSQL = "SELECT username, password, first_name, last_name, email, role FROM USERs WHERE username =:username ";
				var param = [];
				param.push(username.toLowerCase());
				console.log(username)
				var opt = { maxRows: 1, outFormat: oracledb.OBJECT }
				db.doConnect(function(err, connection){  
					if (err) {
						return done(err);
					}
					db.doExecute(connection, selectSQL, param, function(err, result) {
						if (err) {
							db.doRelease(connection);
							return done(err);
						} 
						else {
							if (!result.rows.length) {
								console.log('No user found.')
								db.doRelease(connection);
								return done(null, false, req.flash('loginMessage', 'No user found.'));
							}
							if (password != result.rows[0]['PASSWORD']) {
								console.log('Oops! Wrong password:'+password)
								return done(null, false, req.flash('loginMessage', 'Oops! Invalid credentials.'));
							}
							console.log('Well Done')
							var u = result.rows[0];
							u['PASSWORD'] = null;
							req.session.user = u;
							return done(null, u);
						}
					});
			
				});	
			}
		)
    );
};

