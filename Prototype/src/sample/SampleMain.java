package sample;


import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Harish Bondalapati
 */
public class SampleMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InvalidFormatException {
        XLSX s = new XLSX();
        s.readFile("a.xlsx");
        s.printData();
        s.writeData();
    }
    
}
