package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLSX {
    private Map<Integer,String> stu;
    private Map<Integer,Map<Integer,Integer>> marks;
    private Set<Integer> courses;
        
    public void readFile(String fileName) throws IOException, InvalidFormatException {
        File f = new File(fileName);
        Workbook wb = new XSSFWorkbook(f);
        Sheet sheet = wb.getSheetAt(0);
        
        stu = new TreeMap<Integer,String>();
        marks = new TreeMap<Integer,Map<Integer,Integer>>();
        courses = new TreeSet<Integer>();
        
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);
            Integer stuid = new Double(row.getCell(0).getNumericCellValue()).intValue();
            String stuName = row.getCell(1).getStringCellValue();
            Integer cid = new Double(row.getCell(2).getNumericCellValue()).intValue();
            Character grade = row.getCell(3).getStringCellValue().charAt(0);
            
            if(!stu.containsKey(stuid)){
                stu.put(stuid, stuName);
                marks.put(stuid, new TreeMap<Integer,Integer>());
                marks.get(stuid).put(999999,0);
            }
            int g = 69 - (int)grade;
            marks.get(stuid).put(cid, g);
            marks.get(stuid).put(999999,marks.get(stuid).get(999999)+g);
            courses.add(cid);
        }
    }
    
    public void writeData() throws FileNotFoundException, IOException{
        ArrayList<Integer> cou = new ArrayList<>();
        Workbook res = new XSSFWorkbook();
        Sheet s1 = res.createSheet("Student Courses");
        int rn=0,cn=0;
        Row r=s1.createRow(rn++);
        Cell c = r.createCell(cn++);
        c.setCellValue("919");
        c = r.createCell(cn++);
        c.setCellValue("Name");
        for(Integer i:courses){
            c = r.createCell(cn++);
            c.setCellValue(i);
            cou.add(i);
        }
        c = r.createCell(cn);
        c.setCellValue("GPA");
        for(Entry<Integer,String> student:stu.entrySet()){
            r = s1.createRow(rn++);
            cn=0;
            c = r.createCell(cn++);
            c.setCellValue(student.getKey());
            c = r.createCell(cn++);
            c.setCellValue(student.getValue());
            for(Entry<Integer, Integer> cMarks:marks.get(student.getKey()).entrySet()){
                if(cou.indexOf(cMarks.getKey())!=-1){
                    c = r.createCell(cn+cou.indexOf(cMarks.getKey()));
                    char g = (char)(69 - cMarks.getValue());
                    c.setCellValue(""+g);
                } else {
                    c = r.createCell(cn+cou.size());
                    c.setCellValue(cMarks.getValue()/(marks.get(student.getKey()).size()-1.0));
                }
            }
            
        }
        
        res.write(new FileOutputStream("Result.xlsx"));
    }
    
    public void printData(){
        System.out.println(stu);
        System.out.println(courses);
        System.out.println(marks);
    }
}