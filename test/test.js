//var app = require('../')
var server = require('../app')
var oDb = require('../config/oracledb');
var request = require('request');
const chai = require('chai')
const expect = chai.expect
let chaiHttp = require('chai-http');
let should = chai.should();
var mocks = require('node-mocks-http')
chai.use(chaiHttp);
console.log('Starting test/controllers/')

function buildResponse() {
  return mocks.createResponse({
    eventEmitter: require('events').EventEmitter
  })
}

describe('Login Controller Tests', function () {
  beforeEach(function (done) {
    this.response = mocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })
    done()
  })
  it('GET: /auth/login/', function (done) {
    chai.request(server).get('/auth/login').send().end(function (error, response) {
      console.log('Testing default login GET controller:')
      console.log('Expected Result: 200')
      // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      response.should.have.status(200);
      done()
    });
  })
  it('GET: /auth/forgotPassword/', function (done) {
    chai.request(server).get('/auth/forgotPassword').send().end(function (error, response) {
      console.log('Testing default forgotPassword GET controller:')
      console.log('Expected Result: 200')
      // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      response.should.have.status(200);
      done()
    });
  })
  


  it('POST: logout/', function (done) {
    chai.request(server).post('/').send().end(function (error, response) {
      console.log('Testing default server POST controller:')
     console.log('Expected Result: 404')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      response.should.have.status(404);
      done()
    });

  })

    
  it('Post: /auth/forgotPassword/', function (done) {
    chai.request(server).post('/auth/forgotPassword').send().end(function (error, response) {
      console.log('Testing default forgotPassword GET controller:')
      console.log('Expected Result: 200')
      // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      response.should.have.status(500);
      done()
    });
  })
})
/*describe('Home Controller Tests', function () {
  beforeEach(function (done) {
    this.response = mocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })
    done()
  })
  it('GET: /', function (done) {
    chai.request(server).get('/').send().end(function (error, response) {
      console.log('Testing default server GET controller:')
      console.log('Expected Result: 200')
      // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      response.should.have.status(200);
      done()
    });
  })

  it('POST: login/', function (done) {
    chai.request(server).post('/').send().end(function (error, response) {
      console.log('Testing default server POST controller:')
     console.log('Expected Result: 404')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      response.should.have.status(404);
      done()
    });
  })
})*/
