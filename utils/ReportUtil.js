//"BEGIN DELETEUPLOADDATA(:1); END;"
var LOGGER = require('log4js').getLogger("sms");
var oracledb = require("oracledb");
var db = require("../config/oracledb.js");
var utils = require("./Utilities");
var meta = require("../config/metaData")

var getSheetData = function(reportID, sheet, done){
    var tableName;
    for(var i =0; i < meta['Sheets'].length; i++){
        if(meta['Sheets'][i] == sheet){
            tableName = meta['Tables'][i];
            break;
        }
    }
    console.log(tableName)
    if(tableName){
        var sql = 'select * from '+tableName+' where upload_id = :1';
        utils.getDataObjects(sql,[reportID],function(err, result){
            console.log(err)
            console.log(result.length)
            if(err)
                return done(err,[],meta[sheet+'Sheet'], meta[sheet+'Table']);
            return done(null, result, meta[sheet+'Sheet'], meta[sheet+'Table'])
        })
    } else {    
        console.log("No such sheet as "+sheet)
        return done("NoSheet",null,null,null);
    }
}

var getReportData = function(reportID, reportType, done){
    var reportOptions;
    for(var i =0; i < meta['Reports'].length; i++){
        if(meta['Reports'][i] == reportType){
            reportOptions = meta[meta['Reports'][i]];
            break;
        }
    }
    console.log(reportOptions)
    if(reportOptions){
        var sql = reportOptions.query;
        utils.getDataObjects(sql,[reportID],function(err, result){
            console.log(err)
            console.log(result.length)
            if(err)
                return done(err,[],reportOptions.headings, reportOptions.keys, reportOptions.totals);
            return done(null, result, reportOptions.headings, reportOptions.keys, reportOptions.totals)
        })
    } else {    
        console.log("No such sheet as "+sheet)
        return done("NoSheet",null,null,null);
    }
}

var logAction = function(title, email, url, reportID){
    var sql = "INSERT INTO LOGS (UPDATE_TIME, EMAIL, URL, TITLE) VALUES (sysdate, :1, :2, :3)";
    if(title=='Report'){
        sql = "INSERT INTO LOGS (UPDATE_TIME, EMAIL, URL, TITLE) VALUES (sysdate, :1, :2, (select filename from uploads where upload_id=:3))";
        utils.executeQuery(sql, [email, url, reportID], function(err,result){
            console.log("Inside logAction"+err);
            if(result)
            console.log("Inside logAction"+result.rowsAffected);
        });
    } else {
        utils.executeQuery(sql, [email, url, title], function(err,result){
            console.log("Inside logAction"+err);
            if(result)
            console.log("Inside logAction"+result.rowsAffected);
        });
    }
    
}


module.exports.logAction = logAction
module.exports.getSheetData = getSheetData
module.exports.getReportData = getReportData