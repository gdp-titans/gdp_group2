const crypto = require('crypto');
const Cryptr = require('cryptr');
var LOGGER = require('log4js').getLogger("sms");
var oracledb = require("oracledb");
var db = require("../config/oracledb.js");
var mail = require("../config/sendMail.js");

var hashData = function (data) {
    const hmac = crypto.createHmac('sha256', 'a secret');
    hmac.update(data);
    return hmac.digest('hex');
}

var addNewUser = function (newUser, done) {
    var insertSQL = "Insert into USERS (USERNAME,password,FIRST_NAME,LAST_NAME,EMAIL,ROLE,STATUS) values (:1,:2,:3,:4,:5,:6,'active')";
    var selectSQL = "select username from users where username=:1"
    console.log("in Utils: " + newUser[0])
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError"); }
        db.doExecute(connection, selectSQL, [newUser[0]], function (err, result) {
            if (err) {
                db.doRelease(connection);
                return done("ExecutionError");
            }
            else {
                console.log("In resultsss " + result.rows.length)
                if (result.rows.length > 0) {
                    db.doRelease(connection);
                    console.log("in exists")
                    return done("Exists");
                } else {
                    db.doExecute(connection, insertSQL, newUser, function (err1, result1) {
                        console.log(newUser)
                        if (err1) {
                            db.doRelease(connection);
                            return done("ExecutionError");
                        }
                        else {
                            console.log(result1)
                            db.doRelease(connection);
                            return done("Success");
                        }
                    });
                }
            }
        });
    });
}

var getDataObjects = function (query, params, done) {
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError", null); }
        db.doExecute(connection, query, params, function (err, result) {
            if (err) {
                db.doRelease(connection);
                return done("ExecutionError", null);
            }
            console.log("Results: " + result.rows.length)
            db.doRelease(connection)
            return done(err, result.rows)
        });
    });
}

var updateUser = function (newUser, id, done) {
    var updateSQL = "update users set username = :1,first_name=:2,last_name=:3,role=:4,email=:5 where username = :6";
    var selectSQL = "select username from users where username=:1";
    let tem = [];
    tem.push(newUser[0]); tem.push(newUser[1]); tem.push(newUser[2]); 
    tem.push(newUser[3]); tem.push(newUser[4]); tem.push(id);
    
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError"); }
        db.doExecute(connection, selectSQL, [id], function (err, result) {
            if (err) {
                db.doRelease(connection);
                return done("ExecutionError");
            }
            else {
                console.log("In resultsss " + result.rows.length)
                if (result.rows.length > 0 && id != newUser[0]) {
                    db.doRelease(connection);
                    console.log("in exists")
                    return done("Exists");
                } else {
                    console.log("Inside Execute")
                    db.doExecute(connection, updateSQL, tem, function (err1, result1) {
                        console.log("Temp " + tem)
                        if (err1) {
                            db.doRelease(connection);
                            return done("ExecutionError");
                        }
                        else {
                            console.log(result1)
                            db.doRelease(connection);
                            return done("Success");
                        }
                    });
                }
            }
        });
    });    
}


var updateProfile = function (newUser, id, done) {
    var updateSQL = "update users set username = :1,first_name=:2,last_name=:3 where username = :4";
    var selectSQL = "select username from users where username=:1"
    console.log(id+ " in Utils update profile: " + newUser[0])
    let tem = []
    tem.push(newUser[0]); tem.push(newUser[1]); tem.push(newUser[2]);
    tem.push(id);
    
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError"); }
        db.doExecute(connection, selectSQL, [newUser[0]], function (err, result) {
            if (err) {
                db.doRelease(connection);
                return done("ExecutionError");
            }
            else {
                console.log("In resultsss " + result.rows.length)
                if (result.rows.length > 0 && id != newUser[0]) {
                    db.doRelease(connection);
                    console.log("in exists")
                    return done("Exists");
                } else {
                    console.log("Inside Execute")
                    db.doExecute(connection, updateSQL, tem, function (err1, result1) {
                        console.log("Temp " + tem)
                        if (err1) {
                            db.doRelease(connection);
                            return done("ExecutionError");
                        }
                        else {
                            console.log(result1)
                            db.doRelease(connection);
                            return done("Success");
                        }
                    });
                }
            }
        });
    });    
}


var updatePassword = function(newPwd, oldPWD, id, done){
    var updateSQL = "UPDATE USERS SET PASSWORD = :1, RESET_TOKEN = null WHERE EMAIL = :2 AND PASSWORD = :3";
    console.log(id+ " in Utils update password: '" + newPwd + "', " + oldPWD)
    var temp = [];
    temp.push(newPwd); temp.push(id); temp.push(oldPWD); // 
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError"); }
        db.doExecute(connection, updateSQL, temp, function (err, result) {
            if (err) {
                console.log("in update Error: " + err)
                db.doRelease(connection);
                return done("ExecutionError");
            } else {
                console.log("In update password " + result.rowsAffected)
                if (result.rowsAffected == 0) {
                    db.doRelease(connection);
                    console.log("in IncorrectOld")
                    return done("IncorrectOld");
                } else {
                    console.log("Inside Execute Success")
                    db.doRelease(connection);
                    return done("Success");
                }
            }
        });
    });    
}

var sendForgotPasswordMail = function (email,name) {
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError", null); }
        const hash = crypto.createHash('sha256');
        hash.update(''+Date()+email);
        console.log();
        var token = hash.digest('hex');
        db.doExecute(connection, 'UPDATE USERS SET reset_token = :1 WHERE EMAIL = :2', 
                [token,email], function (err, result) {
            if (err) {
                db.doRelease(connection);
            } else {

                var link = 'http://'+ mail.hostname +'/auth/resetpassword/'+ encryptData("reset", token);
                var html = '<p>Hello '+name+',' +
                           '<br/><br/>A request for password reset has been initated for <b>SSCRA</b> reporting application at '+Date()+'.' +
                           '<br/><br/>Open the below link in a browser to access password reset page. Please provide required details in the webpage and submit details to reset your password.' +
                           '<br/><br/>' +
                           '<a href="'+link+'" >'+link+'</a>' +
                           '<br/><br/>This is an automated mail and replies to this mail will not be monitored. Incase of any issues or queries please contact us using <a><b>Contact Us</b></a> page from the application.' +
                           '<br/><br/>Reagrds,<br/>SSCRA Nortwest</p>';
                mail.sendMail(email,"SSC Reporting Application Account Password Reset",html)

                console.log("Results: " + result.rowsAffected)
                db.doRelease(connection)
            }
        });
    });
}

var executeUpdateQuery = function (query, params, done) {
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError", null); }
        db.doExecute(connection, query, params, function (err, result) {
            if (err) {
                db.doRelease(connection);
                return done("ExecutionError", null);
            }
            console.log("Results: " + result.rowsAffected)
            db.doRelease(connection)
            return done(err, result)
        });
    });
}

var executeQuery = function (query, params, done) {
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError", null); }
        db.doExecute(connection, query, params, function (err, result) {
            db.doRelease(connection)
            return done(err, result)
        });
    });
}


var registerUser = function (newUser, id, done) {
    var updateSQL = "update users set username = :1,first_name=:2,last_name=:3,password=:4,status='active',reset_token=null where reset_token = :5 and username is null";
    var selectSQL = "select reset_token from users where username=:1";
    let tem = [];
    tem.push(newUser[0]); tem.push(newUser[1]); tem.push(newUser[2]); 
    tem.push(newUser[3]); tem.push(id);
    db.doConnect(function (err, connection) {
        if (err) { return done("ConnectionError"); }
        db.doExecute(connection, selectSQL, [newUser[0]], function (err, result) {
            if (err) {
                db.doRelease(connection);
                return done("ExecutionError");
            }
            else {
                console.log("In register " + result.rows.length)
                if (result.rows.length > 0) {
                    db.doRelease(connection);
                    console.log("user in exists")
                    return done("Exists");
                } else {
                    console.log("Inside Execute")
                    db.doExecute(connection, updateSQL, tem, function (err1, result1) {
                        if (err1 || result1.rowsAffected<1) {
                            db.doRelease(connection);
                            return done("ExecutionError");
                        }
                        else {
                            console.log(result1)
                            db.doRelease(connection);
                            return done("Success");
                        }
                    });
                }
            }
        });
    });    
}

var encryptData = function(key,plainData){
    const cryptr = new Cryptr(key);
    const encryptedString = cryptr.encrypt(plainData);
    console.log(encryptedString); 
    return encryptedString;
}

var decryptData = function(key,encryptedData){
    const cryptr = new Cryptr(key);
    try{
        const decryptedString = cryptr.decrypt(encryptedData);
        console.log(decryptedString);
        return decryptedString;
    } catch(err) {
        console.log("Decryption error: " + err);
        return null;
    }
    
}

var sendInvites = function(emails){
    if(emails){
        var mailArray = String(emails).split(',')
        console.log(mailArray)
        var failed = "";
        for(var i = 0; i<mailArray.length; i++){
            let mailId = mailArray[i].trim();
            console.log(i + " "+mailArray[i].trim())
            console.log(mailId)
            getDataObjects("select email from users where upper(email) = upper(:1)", [mailId], function(error, result){
                console.log(error);
                if(result){
                    console.log("Inside invites: "+mailId+", result: "+result.length)
                    if(result.length == 0){
                        const hash = crypto.createHash('sha256');
                        hash.update(''+Date()+mailId);
                        var token = hash.digest('hex');
                        executeUpdateQuery("Insert into USERS values (USERS_SEQ.nextVal,null,null,null,:1,'user','inactive',null,:2)", [mailId, token], function(e, r){
                            console.log(r)
                            if(r && r.rowsAffected>0){
                                var link = 'http://' + mail.hostname + '/manageusers/register/' + encryptData("register", token);
                                var html = '<p>Hello,' +
                                    '<br/><br/>An invitation for SSC Reporting Application(SSCRA) has been sent by administrator to the user associated with this email account, at ' + Date() + '.SSCRA provides access to certain report data that is particularly helpfull to student success and retention center staff.' 
                                    + '<br/><br/> You will be able to access the application once registration process is completed. Open the below link in a browser to access registration page. In the webpage you need to provide basic information and a unique username which can be later used to access SSCRA. please remember the details prrovided in the page for future reference.' +
                                    '<br/><br/>' +
                                    '<a href="' + link + '" >' + link + '</a>' +
                                    '<br/><br/>This is an automated mail and replies to this mail will not be monitored. Incase of any issues or queries please contact us using <a><b>Contact Us</b></a> page from the application.' +
                                    '<br/><br/>Reagrds,<br/>SSCRA Nortwest Adminstrator</p>';
                                mail.sendMail(mailId, "SSC Reporting Application Access Invitation", html)
                            }
                        });
                    } 
                } 
            })
        }
    }
}


module.exports.hashData = hashData
module.exports.addNewUser = addNewUser
module.exports.getDataObjects = getDataObjects
module.exports.updateUser = updateUser
module.exports.updateProfile = updateProfile
module.exports.updatePassword = updatePassword
module.exports.sendForgotPasswordMail = sendForgotPasswordMail
module.exports.executeUpdateQuery = executeUpdateQuery
module.exports.encryptData = encryptData
module.exports.decryptData = decryptData
module.exports.executeQuery = executeQuery
module.exports.registerUser = registerUser
module.exports.sendInvites = sendInvites