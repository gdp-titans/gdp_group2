var XLSX = require('xlsx');
var formidable = require('formidable');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var db = require("../config/oracledb.js");

var parseExcelData = function (workbook, uploadID) {
    var wb = workbook;
    for (var she = 0; she < wb.SheetNames.length; she++) {
        (async () => {
            console.log("Sheet Name: " + wb.SheetNames[she]);
            var sh = wb.Sheets[wb.SheetNames[she]];
            var sheet = (XLSX.utils.sheet_to_json(sh, { header: "A", raw:false }));
            var t = [];
            console.log('Total no of records: ' + sheet.length);
            if (sheet == null) {
                console.log('True');
            } else {
                if (sheet.length > 0) {
                    var columns = Object.keys(sheet[0]);
                    console.log("Total no of columns: " + columns.length);
                    for (var rec = 1; rec < sheet.length; rec++) {
                        var tempA = [];
                        tempA.push(uploadID)
                        for (var c = 0; c < columns.length; c++) {
                            var val = sheet[rec][columns[c]];
                            if (val == null || val == '') tempA.push(null);
                            else tempA.push(val);
                        }
                        t.push(tempA);
                    }
                }
                if (queriesMap[wb.SheetNames[she]] ) {
                    if(wb.SheetNames[she] != 'Contents'){
                        insertSheetData(wb.SheetNames[she], t, uploadID);
                        console.log("Contents: " + t.length)
                    } else {
                        console.log('Inside Contensts')
                        console.log(t)
                    }
                } else {
                    console.log(uploadID + "# Sheets skipped insertion: "+wb.SheetNames[she]);
                }
            }
        })();
    }
    updateFileUploadStatus(uploadID);
}

var generateUploadID = function(done){
    var selectSQL = "select upload_seq.nextVal as UPLOADID from dual"
    db.doConnect(function (err, connection) {
        if (err) { return done(null); }
        db.doExecute(connection, selectSQL, [], function (err, result) {
            console.log("in uploadUtils res: " + result)
            if (err) {
                db.doRelease(connection);
                return done(null);
            }
            else {
                console.log("In results to fetch seq: " + result.rows[0]['UPLOADID'])
                if (result.rows.length > 0) {
                    db.doRelease(connection);
                    return done(result.rows[0]['UPLOADID']);
                } else {
                    db.doRelease(connection);
                    return done(null);
                }
            }
        });
    });
}

var addUploadFile = function(uploadID, filename, username, done){
    var insertSQL = "Insert into UPLOADS (UPLOAD_ID,FILENAME,USERNAME,UPLOAD_TIME,UPDATE_TIME,STATUS) values (:1,:2,:3,sysdate,null,'PENDING')";
    console.log("in uploadUtils: " + filename)
    db.doConnect(function (err, connection) {
        if (err) { return done(null); }
        db.doExecute(connection, insertSQL, [uploadID, filename, username], function (err1, result1) {
            if (err1) {
                db.doRelease(connection);
                return done(null);
            }
            db.doRelease(connection);
            done(result1)
        });
    });
}

var insertSheetData = function(sheetname, data, uploadID){
    let statusSQL = queriesMap['createStatus'];
    let dataSQL = queriesMap[sheetname];
    let updateSQL = queriesMap['updateStatus'];
    db.doConnect(function(err, connection){
        if (err) { console.log("Unable to est connection"); return ;}
        db.doExecute(connection,statusSQL, [uploadID,sheetname], function(err, result){
            console.log("Inserting sheet: "+sheetname);
            if(err) { console.log("Errr: "+err); db.doRelease(connection);  return ;   }
            //console.log(data);
            db.doExecuteMany(connection, dataSQL, data, function(err1, result1){
                var s = "SUCCESS"
                var re = 0
                if (err1) {
                    if(err1.message != 'ORA-24333: zero iteration count')
                        s = "FAILED"; 
                    re = 0;
                    console.log(sheetname+" Error1: " + err1.message+". Actual Records " + data.length)
                } else {
                    console.log("Res: " + result1.rowsAffected)
                    re = result1.rowsAffected;
                }
                db.doExecute(connection,updateSQL, [s, re, uploadID, sheetname], function(err2, result2){
                    if(err2)    console.log("Error2: " + err2);
                    console.log(sheetname+ " Completed. result " + result2.rowsAffected);
                    db.doRelease(connection);  return ;
                })
            })
        })
    })
}

var updateFileUploadStatus = function (uploadID){
    var selectUpload = "select count(*) as PENDING from sheet_uploads where upload_id=:1 and endtime is null"
    var updateQuery = "update uploads set update_time = sysdate, status = 'COMPLETED' where upload_id = :1"
    db.doConnect(function (err, connection) {
        if (err) { console.log("Unable to est connection"); return; }
        db.doExecute(connection, updateQuery, [uploadID], function (err1, result) {
            if (err1)    console.log("Status update error: " + err1);
            console.log("Updating status: " + result)
            db.doRelease(connection); return;
        });
    });
    
};

let queriesMap = {
    'createStatus':"INSERT INTO SHEET_UPLOADS (UPLOAD_ID, SHEET, STARTTIME) VALUES (:1, :2, sysdate)",
    'updateStatus':"UPDATE SHEET_UPLOADS SET ENDTIME = sysdate, STATUS = :1, RECORDS = :2 WHERE upload_id = :3 and sheet = :4",
    
    'Activity':"INSERT INTO ACTIVITY VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26)",
    'Profiles':"INSERT INTO PROFILES VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7, :8, :9, :10)",
    'Appointments':"INSERT INTO APPOINTMENTS VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20)",
    'Office Hours':"INSERT INTO OFFICE_HOURS VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6)",
    'Group Sessions':"INSERT INTO GROUP_SESSIONS VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7)",
    'Events':"INSERT INTO EVENTS VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6)",
    'Plans':"INSERT INTO PLANS VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16)",
    'Prospects':"INSERT INTO PROSPECTS VALUES (:1, :2, , TO_DATE(:3, 'MM/DD/YYYY'), :4, :5, TO_DATE(:6, 'MM/DD/YYYY'), :7, :8, :9, :10)",
    'Intake':"INSERT INTO INTAKE VALUES (:1, :2, TO_DATE(:3, 'MM/DD/YYYY'), :4, :5, :6, :7, :8, :9, :10, :11, :12)",
    'Speed Notes':"INSERT INTO SPEED_NOTES VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7)",
    'Flags':"INSERT INTO FLAGS VALUES (:1, TO_DATE(:2, 'MM/DD/YYYY'), :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13)",
    'Flag Definitions':"INSERT INTO FLAG_DEFINITIONS VALUES (:1, :2, :3, :4, :5, :6)",
    'Attendance':"INSERT INTO ATTENDANCE VALUES (:1, :2, :3, :4, :5, :6, :7, :8, TO_DATE(:9, 'MM/DD/YYYY'), :10, :11, :12, :13, TO_TIMESTAMP(:14, 'MM/DD/YYYY HH.MI AM'))",
    'Student Report':"INSERT INTO STUDENT_REPORT VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, TO_TIMESTAMP(:10, 'MM/DD/YYYY HH.MI AM'), :11, :12, :13, :14, :15, :16, :17, :18, :19, TO_TIMESTAMP(:20, 'MM/DD/YYYY HH.MI AM'), TO_TIMESTAMP(:21, 'MM/DD/YYYY HH.MI AM'), :22, :23, :24, TO_TIMESTAMP(:25, 'MM/DD/YYYY HH.MI AM'))"

}


module.exports.parseExcelData = parseExcelData
module.exports.generateUploadID = generateUploadID
module.exports.addUploadFile = addUploadFile